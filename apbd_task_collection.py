from dotenv import dotenv_values
from update_data.PostgresConnection import PostgresConnection
from update_data.JsonData import JsonData
from update_data import isFullDataExist, insertDataToDb, isPartialDataExist, setFirstIsActive, updateData
import json, sys
from os import listdir

conf=dotenv_values('.env')
stagingPath=''

pg_conn=PostgresConnection(
        user=conf['USERNAME'],
        password=conf['PASSWORD'],
        host=conf['HOST'],
        database=conf['DATABASE']
    )
conn, cursor=pg_conn.connect()

jsondata=JsonData(conf['API'])
jsondata.setApi(conf['API']+'?tahun={}'.format(2020))

def pre_task():
    setFirstIsActive(cursor, conf['DST_SCHEMA'], conf['DST_TABLE'])

def task1():
    # print('getting data...')
    data=jsondata.getDataFromApi()
    # print('splitting data...')
    splittedData=jsondata.splitData(data, 1000)
    # print('exporting data...')
    for sd in splittedData:
        jsondata.export2jsonFile(sd, str(sd['chunk_number'])+'.json', 'data-result')

def task2():
    for File in listdir(stagingPath):
        with open(stagingPath+'/'+File, 'r') as jsonfile:
            finalResult=list()
            data=json.load(jsonfile).get('data')
            for d in data:
                resultIsFullDataExist=isFullDataExist(cursor, conf['DST_SCHEMA'], conf['DST_TABLE'], d)
                if resultIsFullDataExist[0]:
                    for r in resultIsFullDataExist[1]:
                        r=jsondata.setDataTimeUpdated(r); r=jsondata.setIsActive(r)
                        updateData(cursor, conf['DST_SCHEMA'], conf['DST_TABLE'], r, eval(conf['LIST_COLUMNS']))
                        conn.commit()
                else:
                    resultIsPartialDataExist=isPartialDataExist(cursor, conf['DST_SCHEMA'], conf['DST_TABLE'], d, eval(conf['LIST_COLUMNS']))
                    if resultIsPartialDataExist[0]:
                        for r in resultIsPartialDataExist[1]:
                            d['jumlah_perubahan']=r['jumlah_perubahan']+1; d=jsondata.setDataTimeUpdated(d); d=jsondata.setIsActive(d)
                            finalResult.append(d)
                    else:
                        d=jsondata.setDataTimeCreated(d); d=jsondata.setDataTimeUpdated(d); d=jsondata.setIsActive(d); d['jumlah_perubahan']=0
                        finalResult.append(d)
        JsonData.export2jsonFile(JsonData.splitData(finalResult, chunk_size=1000)[0], File, stagingPath)

def task3():
    for File in listdir(stagingPath):
        with open(stagingPath+'/'+File, 'r') as jsonfile:
            data=json.load(jsonfile).get('data')
            for d in data:
                insertDataToDb(cursor, conf['DST_SCHEMA'], conf['DST_TABLE'], d)
                conn.commit()