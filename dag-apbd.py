import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
import sys

sys.path.insert(1, '')
stagingPath=''

from apbd_task_collection import task1, task2, task3, pre_task

args={
    'owner':'jds-data-engineer',
    'start_date':datetime.datetime(2020, 1, 22),
    'retries':3,
    'retry_delay':timedelta(minutes=10)
}

dag=DAG(
    dag_id='apbd-update',
    default_args=args,
    description='Update APBD data daily.',
    schedule_interval='1 0 * * *'
)

# pt1=BashOperator(
#     task_id='activate-environment',
#     bash_command='conda activate apbd',
#     dag=dag
# )

pt=PythonOperator(
    task_id='set-isactive-to-false',
    python_callable=pre_task,
    dag=dag
)

t1=PythonOperator(
    task_id='get-data',
    python_callable=task1,
    dag=dag
)

t2=PythonOperator(
    task_id='conditional-check-and-transform-data',
    python_callable=task2,
    dag=dag
)

t3=PythonOperator(
    task_id='store-into-database',
    python_callable=task3,
    dag=dag
)

pst=BashOperator(
    task_id='remove-all-in-staging',
    bash_command='rm {}/*'.format(stagingPath),
    dag=dag
)


# dependencies
pt >> t1 >> t2 >> t3 >> pst