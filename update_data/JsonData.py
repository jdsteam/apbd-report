from update_data import isEmpty, dict2ColsRows
import requests, datetime, time, json

class JsonData:
    def __init__(self, api):
        self.api=api
        self.time_created=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        
    def getDataFromApi(self):
        try:
            data=requests.get(self.api).json().get('data')
            if isEmpty(data):
                return self.getDataFromApi()
            else:
                return data
        except:
            time.sleep(10)
            return self.getDataFromApi()

    def setApi(self, args:str):
        self.api=args

    def setDataTimeCreated(self, data):
        data['time_created']=self.time_created
        return data

    def setDataTimeUpdated(self, data):
        data['time_updated']=self.time_created
        return data

    def setIsActive(self, data):
        data['is_active']=True
        return data

    @classmethod
    def splitData(cls, data, chunk_size, start_at=0):
        data_length=len(data)
        result=[]
        chunk_number=0
        end_at=chunk_size
        while start_at<=data_length:
            splittedData={'chunk_number':chunk_number, 'data':data[start_at:end_at]}
            result.append(splittedData)
            start_at+=chunk_size
            end_at+=chunk_size
            chunk_number+=1
        return result

    @classmethod
    def export2jsonFile(cls, data, fileName, pathFile=''):
        with open(pathFile+'/'+fileName, 'w') as jsonfile:
            json.dump(data, jsonfile)