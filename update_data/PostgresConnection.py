import psycopg2
from psycopg2.extras import RealDictCursor

class PostgresConnection:
    def __init__(self, user, password, host, database, port=5432):
        self.user=user
        self.password=password
        self.host=host
        self.dbname=database
        self.port=port

    def connect(self):
        try:
            conn=psycopg2.connect(
                user=self.user,
                password=self.password,
                host=self.host,
                database=self.dbname,
                port=self.port
            )
            cursor=conn.cursor(cursor_factory=RealDictCursor)
            return conn, cursor
        except psycopg2.DatabaseError as err:
            print(err)
            return self.connect()