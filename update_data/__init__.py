import datetime, json

def isEmpty(data: list):
    if len(data)==0:
        return True
    else:
        return False

def dict2ColsRows(data: dict):
    cols=', '.join([col for col in data.keys()])
    vals=', '.join(["'{}'".format(val.replace("'", '')) if isinstance(val, str) and val!='null' \
        else 'null' if val==None \
        else "{}".format(val) for val in data.values()])
    return cols, vals

def dict2WhereStatement(data:dict):
    whereStatement=' and '.join('\"{}\"=\'{}\''.format(key, val) \
        if isinstance(val, str) \
        else '\"{}\"=null'.format(key) if val==None \
        else '\"{}\"={}'.format(key, val) for key, val in data.items())
    return whereStatement

def insertDataToDb(cursor, schema, table, data: dict):
    cols, rows=dict2ColsRows(data)
    query='''
        insert into "{}"."{}"({}) values ({}) on conflict do nothing;
    '''.format(schema, table, cols, rows)
    cursor.execute(query)

def getDataFromDb(cursor, query:str):
    cursor.execute(query)
    data=cursor.fetchall()
    return data

def getTableColumns(cursor, schema, table):
    query='''
        select column_name
        from information_schema.columns
        where table_schema = '{}'
            and table_name   = '{}';
    '''.format(schema, table)
    cursor.execute(query)
    cols=[col[0] for col in cursor.fetchall()]
    return cols

def isPartialDataExist(cursor, schema, table, data:dict, cols:list):
    if set(cols).issubset(set(data.keys())):
        tmpData={col:data[col] for col in data.keys()}
        whereStatement=dict2WhereStatement(tmpData)
        query='''
            select * 
            from "{}"."{}"
            where {};
        '''.format(schema, table, whereStatement)
        result=getDataFromDb(cursor, query)
    if result:
        return True, result
    else:
        return False, None

def isFullDataExist(cursor, schema, table, data:dict):
    whereStatement=dict2WhereStatement(data)
    query='''
        select * from "{}"."{}"
        where {}
    '''.format(schema, table, whereStatement)
    result=getDataFromDb(cursor, query)
    if result:
        return True, result
    else:
        return False, None

def setFirstIsActive(cursor, schema, table):
    query='''
        update "{}"."{}"
        set "is_active" = false;
    '''.format(schema, table)
    cursor.execute(query)

def updateData(cursor, schema, table, data:dict, cols:list):
    setStatement=dict2WhereStatement(data).replace(' and ', ', ')
    cols2WhereStatement=dict()
    for col in cols:
        cols2WhereStatement[col]=data[col]
    whereStatement=dict2WhereStatement(cols2WhereStatement)
    query='''
        update "{}"."{}"
        set {}
        where {};
    '''.format(schema, table, setStatement, whereStatement)
    cursor.execute(query)